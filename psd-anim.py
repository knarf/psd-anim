#!/usr/bin/env python3

from psd_tools import PSDImage
import re
import PIL
import math
import argparse
import os.path
import sys

def is_group(layer):
    try:
        layer.layers
    except AttributeError:
        return False
    return True

def frame_number(layer):
    r = re.search(r'f(?:rame)?\s*(\d+)', layer.name, flags=re.IGNORECASE)
    if r:
        return int(r.group(1))
    return None

def export_sheet(psd, layers, output, nb_col=None):
    fw = psd.header.width
    fh = psd.header.height
    nb_frame = len(layers)

    if nb_col is None:
        nb_col = round(math.sqrt(nb_frame))

    nb_row = nb_frame//nb_col + 0

    img = PIL.Image.new("RGBA", (nb_col*fw, nb_row*fh), color=(255, 255, 255, 0))

    for i, e in enumerate(layers):
        x = (i % nb_col) * fw
        y = (i // nb_col) * fh
        img.paste(e.as_PIL(), (x + e.bbox.x1, y + e.bbox.y1))

    img.save(output)

def export_multi(psd, layers, prefix):
    for i, e in enumerate(layers):
        r = PIL.Image.new("RGBA", (psd.header.width, psd.header.height), color=(255, 255, 255, 0))
        r.paste(e.as_PIL(), (e.bbox.x1, e.bbox.y1))
        r.save("{0}_{1:02d}.png".format(prefix, i))

def reportEvent(event):
    print('keysym={0}, keysym_num={1}'.format(event.keysym, event.keysym_num))


# PREVIEWER
from gi.repository import Gtk, GObject, GdkPixbuf
class Viewer(Gtk.Window):

    def __init__(self, pixbufs):
        Gtk.Window.__init__(self, title="Hello World")
        self.pb = pixbufs
        self.pbi = 0
        self.img = Gtk.Image()
        self.img.set_from_pixbuf(self.pb[self.pbi])
        self.add(self.img)
        self.timeout = GObject.timeout_add(50, self.next_cb, None)

    def next_cb(self, widget):
        self.pbi = (self.pbi+1) % len(self.pb)
        self.img.set_from_pixbuf(self.pb[self.pbi])
        return True


def preview(psd, layers):
    win = Gtk.Window()
    pixbufs = []

    for i, e in enumerate(layers):
        r = PIL.Image.new("RGBA", (psd.header.width, psd.header.height), color=(255, 255, 255, 0))
        r.paste(e.as_PIL(), (e.bbox.x1, e.bbox.y1))
        fn = "{0}_{1:02d}.png".format("/tmp/img", i)
        r.save(fn)
        pb = GdkPixbuf.Pixbuf.new_from_file(fn)
        pixbufs.append(pb)

    win = Viewer(pixbufs)
    win.connect("delete-event", Gtk.main_quit)
    win.show_all()
    Gtk.main()
    sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Export sprite animations from any Photoshop PSD file.')
    parser.add_argument('-e', '--export', help='type of export', choices=['sheet','multi'], default='sheet')
    parser.add_argument('-c', '--nbcol', help='number of column (sheet)', type=int, default=None)
    parser.add_argument('-o', '--output', help='output file (sheet) or prefix (multi)', default='')
    parser.add_argument('-p', '--preview', help='prefiew animation', action='store_true')
    parser.add_argument('file', help='PSD file to process', type=argparse.FileType('rb'))
    args = parser.parse_args()
    fname = args.file.name
    psd = PSDImage.load(fname)
    frame_layers = []

    for e in psd.layers:
        if is_group(e) and frame_number(e) is not None and e.visible:
            frame_layers.append(e)

    frame_layers.sort(key=frame_number)

    if len(frame_layers) == 0:
        raise Exception("No animation frames found in '{0}'".format(fname))

    if args.preview:
        preview(psd, frame_layers)
        sys.exit(0)

    if args.export == 'sheet':
        if not args.output:
            path, ext = os.path.splitext(fname)
            args.output = path + '.png'

        export_sheet(psd, frame_layers, args.output, args.nbcol)

    elif args.export == 'multi':
        if not args.output:
            path, ext = os.path.splitext(fname)
            args.output = path

        export_multi(psd, frame_layers, args.output)

    else:
        raise Exception('invalid export type')
