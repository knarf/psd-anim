# psd-anim

Work on your animation from photoshop.

This script iterates over your PSD layer groups which have a frame
number in their name ("frame 1", "f1", etc) and renders each group
either as a spritesheet or as multiple images.

Layers that do not belong in a frame layer group are ignored.



    usage: psd-anim.py [-h] [-e {sheet,multi}] [-c NBCOL] [-o OUTPUT] [-p] file

    Export sprite animations from any Photoshop PSD file.

    positional arguments:
      file                  PSD file to process

    optional arguments:
      -h, --help            show this help message and exit
      -e {sheet,multi}, --export {sheet,multi}
                            type of export
      -c NBCOL, --nbcol NBCOL
                            number of column (sheet)
      -o OUTPUT, --output OUTPUT
                            output file (sheet) or prefix (multi)
      -p, --preview         prefiew animation
